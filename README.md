# INTRODUCTION

Ship logs to [Logz.io](https://logz.io/) via Monolog. This glue module allows the
shipping handler provided by the [logzio-monolog](https://github.com/inpsyde/logzio-monolog)
library to be exposed to the [Drupal Monolog module](https://www.drupal.org/project/monolog)
and configured using the Drupal config system.

# Features

- Registers a `monolog.handler.logz` service that can be then configured into
  the monolog service.
- Sources handler configuration from the Drupal configuration system under a
  `monolog_logz.settings` configuration item.

Due to [#2951046: Allow parsing and writing PHP constants and enums in YAML files](https://www.drupal.org/project/drupal/issues/2951046),
it is not currently possible to configure the logzio-monolog library's handler
directly in a Drupal `monolog-services.yml` file as the library specifies
[PHP Enumerations](https://www.php.net/manual/en/language.enumerations.php) as
[arguments](https://github.com/inpsyde/logzio-monolog/blob/master/src/Handler/LogzIoHandler.php#L36-L43)
and Drupal is not setting the YAML parser flags in a way that allows these typed
arguments.

Even if passing enums in a Drupal `services.yml` were possible, it is a common
pattern to configure site-specific values in `settings.php` or the Drupal config
system. Some users may not wish to hard-code their Logz.io token in a
`*.services.yml` file but rather inject it via the environment or a
secrets-management system.

# Setup and Configuration

Like the Monolog module, this module does not have any visible user interface
and is configured by adding values to the Drupal configuration, usually in
`settings.php`. Additionally you must tell monolog to use it in a
`monolog.services.yml` (or equivalent) file used to configure monolog (as
described in the [Monolog module's README](https://git.drupalcode.org/project/monolog/blob/HEAD/README.md)).

## Basic configuration

1. Install this module (and the Monolog module) with composer to ensure you are also getting the logzio-monolog library.
2. Add your configuration details to your `settings.php`:

        // Part of the Monolog module setup:
        $settings['container_yamls'][] = 'sites/default/monolog.services.yml';

        // Configure the Logz.io shipping handler:
        $config['monolog_logz.settings']['token'] = 'my-logz.io-token';
        $config['monolog_logz.settings']['level'] = 'Debug';
        $config['monolog_logz.settings']['host'] = 'UsEast1';

3. Configure the logz handler provided by this module into the monolog service
   in a `monolog.services.yml`:

        parameters:
          monolog.channel_handlers:
            php: ['error_log', 'logz']
            default: ['logz']

## Additional configuration

### Logz.io Host
Your Logz.io account will be associated with a particular host where your logs
are stored. You'll need to configure this host so that you ship them to the
correct location that knowns about your account.

Options for the `host` parameter can be seen in
[inpsyde/logzio-monolog/src/Enum/Host.php](https://github.com/inpsyde/logzio-monolog/blob/master/src/Enum/Host.php).
Use a string that matches the enum's name in your Drupal configuration like:

    $config['monolog_logz.settings']['host'] = 'EuCentral1';

### Log Level

The Monolog log levels are an enum defined in [Seldaek/monolog/src/Monolog/Level.php](https://github.com/Seldaek/monolog/blob/main/src/Monolog/Level.php).
You can use any string that will map to one of these log levels in its
[`Level::fromName($name)`](https://github.com/Seldaek/monolog/blob/main/src/Monolog/Level.php#L84-L99)
method.

    // These are all equivalent.
    $config['monolog_logz.settings']['level'] = 'Warning';
    $config['monolog_logz.settings']['level'] = 'warning';
    $config['monolog_logz.settings']['level'] = 'WARNING';



### Log Type

Logz.io uses the `type` field on logs to allow both categorization of entries as
well as triggering of various [Sawmill ingestion pipelines](https://docs.logz.io/docs/category/log-parsing)
that allow further parsing and modification of log entries.

If unspecified the default type sent will be `drupal`, but this can be customized
to any string you wish:

    $config['monolog_logz.settings']['type'] = 'drupal_watchdog';

### Bubble

As described in the [Monolog documentation](https://github.com/Seldaek/monolog/blob/main/doc/01-usage.md#core-concepts),
handlers can chose the behavior of how events cascade after they are handled:
> Handlers also have a `$bubble` property which defines whether they block the
record or not if they handled it.

If desired you can change the `bubble` value from its default of `TRUE` to`FALSE`:

    $config['monolog_logz.settings']['bubble'] = FALSE;

### SSL

The `ssl` option [determines](https://github.com/inpsyde/logzio-monolog/blob/master/src/Handler/LogzIoHandler.php#L49-L51)
if log entries will be shipped to Logz.io's https server or http server and
which port is used for transport.

If desired you can change the `ssl` value from its default of `TRUE` to`FALSE`:

    $config['monolog_logz.settings']['ssl'] = FALSE;

## Configuration examples

1. Send PHP errors to a local file and Logz and Drupal messages to syslog and Logz:

        # in monolog.services.yml
        parameters:
          monolog.channel_handlers:
            php: ['error_log', 'logz']
            default: ['syslog', 'logz']

2. Send errors only to Logz:

        # in monolog.services.yml
        parameters:
          monolog.channel_handlers:
            php: ['logz']
            default: ['logz']

3. As above, but annotate with the hostname of the server:

        # in monolog.services.yml
        parameters:
          monolog.channel_handlers:
            php: ['logz']
            default: ['logz']
          monolog.logger.processors: ['server_host']

# Additional Requirements

- [Drupal Monolog module](https://www.drupal.org/project/monolog) (installed as a dependency with composer)
- [logzio-monolog](https://github.com/inpsyde/logzio-monolog)
library (installed as a dependency with composer)
- An account and subscription at [Logz.io](https://logz.io/) to send the logs to.
  - A [log-shipping token](https://docs.logz.io/docs/user-guide/admin/authentication-tokens/log-shipping-tokens)
    associated with your account.
