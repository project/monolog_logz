<?php

namespace Drupal\monolog_logz\Handler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Inpsyde\LogzIoMonolog\Handler\LogzIoHandler as InsydeLogzIoHandler;
use Monolog\Handler\HandlerWrapper;
use Monolog\Level;

/**
 * Wrapper to allow configure the Handler from Drupal configuration values.
 */
class LogzIoHandler extends HandlerWrapper {

  /**
   * Constructs a new LogzIoHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $config = $configFactory->get('monolog_logz.settings');

    $token = $config->get('token');
    if (empty($token)) {
      throw new \Exception('A monolog_logz.settings must have a value set for "token".');
    }

    $type = $config->get('type');
    if (empty($type)) {
      $type = 'drupal';
    }

    $ssl = $config->get('ssl');
    if (is_null($ssl) || $ssl === '') {
      $ssl = TRUE;
    }

    $levelName = $config->get('level');
    if (empty($levelName)) {
      $levelName = 'Debug';
    }
    $level = Level::fromName($levelName);

    $bubble = $config->get('bubble');
    if (is_null($bubble) || $bubble === '') {
      $bubble = TRUE;
    }

    $hostName = $config->get('host');
    if (empty($hostName)) {
      $hostName = 'UsEast1';
    }
    $host = constant("\Inpsyde\LogzIoMonolog\Enum\Host::$hostName");

    parent::__construct(new InsydeLogzIoHandler(
      $token,
      $type,
      $ssl,
      $level,
      $bubble,
      $host
    ));
  }

}
